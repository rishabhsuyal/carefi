import React from 'react';
import { IonSlides, IonSlide, IonContent } from '@ionic/react';
import '../GalleryBlock/GalleryBlock.css'


const slideOpts = {
    initialSlide: 1,
    speed: 400
};

function SliderLayer({ data }) {
    return (
        <IonContent>
            <IonSlides pager={true} options={slideOpts}>

                {
                    data.map((item) =>
                        <IonSlide>
                                <img src={item.urls.regular} alt="" />
                        </IonSlide>
                    )
                }


            </IonSlides>
        </IonContent>
    )
}

export default SliderLayer;