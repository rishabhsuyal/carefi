import React from 'react'
import './GalleryBlock.css'
import {
  IonAccordion,
  IonAccordionGroup,
  IonItem,
  IonButton
} from '@ionic/react';
import { caretDownCircleOutline } from 'ionicons/icons';

function GalleryBlock({ data }) {

  const url= data.urls.small;
  const username=data.user.username

  return (
    <div className='image-div'>
      <img className='gallery_block' src={url} alt="" />
      <div className='accodian-div'>
        <IonAccordionGroup>
          <IonAccordion value="first" toggleIcon={caretDownCircleOutline} toggleIconSlot="start">
            <IonItem toggleIcon="" slot="header" >
              {username}
            </IonItem>
            <div className="ion-padding" slot="content">
              <p>Created At  {data.created_at}</p>
              <p>Likes {data.likes}</p>
              <p>ID  {data.id}</p>
            </div>
          </IonAccordion>
        </IonAccordionGroup>
      </div>

      

    </div>


  )
}

export default GalleryBlock

