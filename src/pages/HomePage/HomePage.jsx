import React, { useState } from 'react';
import { IonSearchbar,IonPage, IonContent, IonRefresher, IonRefresherContent } from '@ionic/react';
import axios from 'axios'
import './HomePage.css'
import Gallery from '../../components/Gallery/Gallery';
import SliderLayer from '../../components/SliderLayer/SliderLayer';


function HomePage(){
  
  const [page,setpage] = useState(1);
  const [searchText,setsearchText] = useState("");

  const [data, setdata] = useState([])
  let cancelToken;
  async function handleChange(ev){
    const text=ev.detail.value
    
    if(cancelToken){
        cancelToken.cancel("CANCEL CALL")
    }
    cancelToken=axios.CancelToken.source();
    let result;
    try{
    result = await axios.get(`https://api.unsplash.com/search/photos?page=${page}&per_page=20&query=${text}&client_id=PIxXvSykAV9RSBxDTMyyJinI6OwNh4g7sPcN6nI1-1Q`,{cancelToken:cancelToken.token})
    setdata(result.data.results);    
    }catch(err){
        console.log(err);
    }
    console.log(result);
    setsearchText(text);
    
  }
  function handleNext(k){

    if(page+k<=0) setpage(1)
    else setpage(page+k);
    console.log(searchText,page);
    axios.get(`https://api.unsplash.com/search/photos?page=${page}&per_page=20&query=${searchText}&client_id=PIxXvSykAV9RSBxDTMyyJinI6OwNh4g7sPcN6nI1-1Q`)
    .then((response)=>setdata(response.data.results))
    .catch((err)=>console.log(err))
  }

  function doRefresh(event) {
    console.log('Begin async operation');
  
    setTimeout(() => {
      handleNext(1);
      event.detail.complete();
    }, 2000);
  }
  return (
<IonContent>
<IonRefresher slot="fixed" onIonRefresh={doRefresh} pullFactor={0.5} pullMin={100} pullMax={200}>
  <IonRefresherContent>
  </IonRefresherContent>
</IonRefresher>
<IonPage>
      <IonSearchbar debounce={1000} onIonChange={(ev) =>handleChange(ev)}></IonSearchbar>
        <Gallery data={data} handleN={handleNext}/>
        <SliderLayer data={data}/>
    </IonPage>
</IonContent>
  );
};

export default HomePage;